import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.posts = [
      {
        title: 'Primeira Reunião',
        description: 
        `Fiz minha primeira reunião com a Ligia, e me foi passado que eu deveria estudar os sites dos núcleos
	argentino e estadunidense`,
        date: '26/08/2018'
      },
      {
        title: 'Primeiro wireframe',
        description: `Apresentei o primeiro wireframe para a Ligia, obtive o feedback e mudei mais um pouco`,
        date: '05/09/2018'
      },
      {
        title: 'Primeira iteração de implementação',
        description: `Implementei os componentes de NavBar e Footer, e usei eles parem construir a página incial`,
        date: '15/09/2018'
      },
      {
        title: 'Feedback da primeira iteração',
        description: `Mostrei o resultado de ontem para a Ligia e obtive feedback positivo, e mais orientações
	de como construir o layout final`,
        date: '16/09/2018'
      },
      {
        title: 'Reformulação da página inicial',
        description: `Apliquei as mudanças que a Ligia me pediu na página inicial`,
        date: '22/09/2018'
      },
      {
        title: 'Configuração do deploy',
        description: `Coloquei o código no GitLab, e configuei o Netlify para realizar o deploy automático
	assim que qualquer commit chegar no repositório`,
        date: '02/10/2018',
      },
      {
        title: 'Volta ao desenvolvimento',
        description: `Voltei a desenvolver o site, mudei as configurações do linter, adicionei algumas
	transições, e organize os arquivos do repositório`,
        date: '06/11/2018'
      },
      {
        title: 'Página de times',
        description: `Criei o layout inicial da página de times, adicionei a opçõa de colocar imagens de
	      fundo no componente banner. E mandei as novas páginas para a Ligia me mandar mais sugestões`,
        date: '06/11/2018'
      },
      {
        title: 'Página de lista de projetos',
        description: `Adicionei a página de lista de projetos baseados no layout argentino, e novamente
	mandei as imagens para a Ligia avaliar`,
        date: '07/11/2018'
      },
      {
        title: 'Sugestões aplicadas',
        description: `Apliquei as mudanças sugeridas pela Ligia das duas últimas adições`,
        date: '13/11/2018'
      },
      {
        title: 'Página de detalhes de projeto',
        description: `Adicionei a página de detalhes de projeto, e aproveitei que a Ligia estava livre
	      e colotei e apliquei as sugestões antes de commitar. Também consertei alguns erros de layout
	      na página de times.`,
        date: '19/11/2018'
      },
      {
        title: 'Arrumação de código',
        description: `Consertei todos os erros de estilo no código fonte.`,
        date: '20/11/2018'
      },
      {
        title: 'Início do estudo das APIs de pagamento',
        description: `Comecei a estudar a API de pagamentos do PagSeguro, para evitar que o usuário saia do
	      site para realizar uma doação`,
        date: '23/11/2018'
      },
      {
        title: 'Fim do estudo das APIs de pagamento',
        description: `Descobri que a API de pagamentos do PagSeguro só funciona através de requisições vindas
	      de um servidor, e que o PayPal não disponibiliza outras opções além do botão embutido para doações.`,
        date: '25/11/2018'
      },
      {
        title: 'Página de Doação',
        description: `Adicionei o esqueleto da página de doação, e expliquei para a Ligia sobre as APIs de pagamento`,
        date: '26/11/2018'
      },
      {
        title: 'Finalização da página de doação',
        description: `Apliquei as sugestões dadas pela Ligia, e finalizei o layout da página de doações. Estou
	      no aguardo para que ele me envie os botões a serem embutidos.`,
        date: '27/11/2018'
      },
    ];

    this.itemRender = this.itemRender.bind(this);
  }

  linkRender(post) {
    if (post.link !== undefined) 
      return (
        <a href={post.link}> <div className="post-link"> { post.link } </div></a>
      );
    return;
  }

  itemRender() {
    return this.posts.map(post => {
      return (
        <div className="post" key={ post.title }>
          <div className="post-title"> { post.title } </div>
          <div className="post-description"> { post.description } </div>
          { this.linkRender(post) }
          <div className="post-date"> { post.date } </div>
        </div>
      );
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">MAC0213 - Reimplementação do site Engenheiros Sem Fronteiros SP </h1>
          <h2 className="App-title">
            Leonardo Lana - 
            Supervisionado por Ligia Monteiro
          </h2>
        </header>

        <div className="post-list">
          { this.itemRender() }
        </div>
      </div>
    );
  }
}

export default App;
